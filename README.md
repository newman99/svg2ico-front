# svg2ico-front

> A Vue.js front-end for SVG2ICO.

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

# Upload to S3
```
$ aws s3 cp dist s3://<bucket_name> --acl public-read --recursive --profile=<profile_name>
```

# Clear CloudFront cache
```
$ aws cloudfront create-invalidation --distribution-id <distribution-id> --paths "/*" --profile=<profile_name>
```
