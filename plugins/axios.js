import axios from 'axios'

export default axios.create({
  baseURL: process.env.API_URL,
  headers: { 'X-Api-Key': process.env.API_TOKEN }
})
